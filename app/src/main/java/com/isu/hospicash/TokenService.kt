package com.isu.hospicash

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Url

interface TokenService {
    @POST
    fun loginToken(
        @Body tokenrequestBody: TokenRequestBody, @Url url: String,
    ): Call<TokenResponse>

    @FormUrlEncoded
    @POST
    fun requestAccessToken(
        @Header("Authorization") authHeader: String,
        @Header("Content-Type") contentType: String,
        @Header("User-Agent") user_agent : String = "Android",
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String,
        @Field("grant_type") grantType: String,
        @Url url:String,
    ): Call<TokenResponse>
}