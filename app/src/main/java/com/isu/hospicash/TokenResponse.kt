package com.isu.hospicash

import com.google.gson.annotations.SerializedName

data class TokenResponse(

    @field:SerializedName("access_token")
    val accessToken: String? = null,

    @field:SerializedName("redirectUri")
    val redirectUri: String? = null,

    @field:SerializedName("adminName")
    val adminName: String? = null,

    @field:SerializedName("refresh_token")
    val refreshToken: String? = null,

    @field:SerializedName("bankCode")
    val bankCode: String? = null,

    @field:SerializedName("mobileNumber")
    val mobileNumber: Long? = null,

    @field:SerializedName("created")
    val created: Long? = null,

    @field:SerializedName("scope")
    val scope: String? = null,

    @field:SerializedName("token_type")
    val tokenType: String? = null,

    @field:SerializedName("expires_in")
    val expiresIn: Int? = null,

    @field:SerializedName("jti")
    val jti: String? = null
)
