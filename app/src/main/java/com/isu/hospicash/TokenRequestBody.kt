package com.isu.hospicash

import com.google.gson.annotations.SerializedName

data class TokenRequestBody(
    @SerializedName("username" ) var username : String? = null,
    @SerializedName("password" ) var password : String? = null,
    @SerializedName("grant_type" ) var grantType : String? = null
)
