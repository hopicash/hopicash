package com.isu.hospicash

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.isu.hospicashinapp.HospicashLaunchActivity
import com.isu.hospicashinapp.HospicashRequiredData
import com.isu.hospicashinapp.ui.theme.HospicashTheme
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            HospicashTheme {

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val username = remember{
                        mutableStateOf("sumantakumar")
                    }
                    val password = remember{
                        mutableStateOf("Test@2023")
                    }
                    Column(
                        modifier = Modifier.fillMaxSize(),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {

                        TextField(value =username.value , onValueChange = {
                            username.value = it
                        }, singleLine = true, label = {
                            Text(text = "UserName")
                        },modifier = Modifier.padding(10.dp))

                        TextField(value =password.value , onValueChange = {
                            password.value = it
                        }, singleLine = true, label = {
                            Text(text = "password")
                        },modifier = Modifier.padding(10.dp))

                        Button(onClick = {
                            loginUsingOAuth(userName = username.value.trim(),password = password.value.trim())
                        }) {
                           Text(text = "click me")
                        }


                    }

                }
            }
        }
    }
    private fun loginUsingOAuth(userName: String, password: String) {
        val oAuthApi = Retrofit.Builder()
            .baseUrl("https://oauth2-auth-server-oauthjwt-prod.iserveu.tech")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(TokenService::class.java)

        val request = oAuthApi.requestAccessToken(
            authHeader=
            "Basic Y29tbW9uLWFkbWlucy1vYXV0aDItY2xpZW50OmNvbW1vbi1hZG1pbnMtb2F1dGgtcGFzc3dvcmQ=",
            contentType = "application/x-www-form-urlencoded",
            user_agent = "Android",
            username = userName,
            password = password,
            clientId = "common-admins-oauth2-client",//"common-admins-oauth2-client",
            clientSecret = "common-admins-oauth-password",//"common-admins-oauth-password",
            grantType = "password",
            url = "https://authorization-server-common-admin-uat-twdwtabx5q-el.a.run.app/oauth/token"
        )

        request.enqueue(object: Callback<TokenResponse> {
            override fun onResponse(call: Call<TokenResponse>, response: Response<TokenResponse>) {
                try {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            val intent =
                                Intent(this@MainActivity, HospicashLaunchActivity::class.java)


                            HospicashRequiredData.access_token = response.body()?.accessToken
                            HospicashRequiredData.adminName = response.body()?.adminName
                            HospicashRequiredData.bankCode = response.body()?.bankCode
                            HospicashRequiredData.mobileNumber =
                                response.body()?.mobileNumber.toString()

                            startActivity(intent)
                        } else {
                            Toast.makeText(this@MainActivity, "failed2", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(this@MainActivity, "failed3", Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Toast.makeText(this@MainActivity,"exception $e",Toast.LENGTH_LONG).show()
                }
            }

            override fun onFailure(call: Call<TokenResponse>, t: Throwable) {
                Toast.makeText(this@MainActivity, "failed1", Toast.LENGTH_SHORT).show()
            }

        })
    }
}



