package com.isu.hospicashinapp

import android.annotation.SuppressLint
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.isu.hospicashinapp.data.model.req.CreateOrderRequest
import com.isu.hospicashinapp.data.model.req.OrderStatusRequest
import com.isu.hospicashinapp.data.model.req.PincodeFetchRequest
import com.isu.hospicashinapp.data.model.req.ProductDetailsRequest
import com.isu.hospicashinapp.data.model.req.SendOtpRequestBody
import com.isu.hospicashinapp.data.model.req.ValidateOtpRequestBody
import com.isu.hospicashinapp.data.model.res.CreateOrderResponse
import com.isu.hospicashinapp.data.model.res.OrderStatusResponse
import com.isu.hospicashinapp.data.model.res.OtpResponseBody
import com.isu.hospicashinapp.data.model.res.ProductCoverAmountResponse
import com.isu.hospicashinapp.data.model.res.ProductDetailsResponse
import com.isu.hospicashinapp.domain.Repository
import com.isu.hospicashinapp.presenter.Gender
import com.velox.lazeir.utils.outlet.handleFlow
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

val ioScope = CoroutineScope(Dispatchers.IO)
val mainScope = CoroutineScope(Dispatchers.Main)

@HiltViewModel
class HospicashViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    //global
    val loading = mutableStateOf(false)
    val failureMessage = mutableStateOf("")
    val failureStatus = mutableStateOf(false)

    val productDetails = mutableStateOf<ProductDetailsResponse?>(null)
    val orderStatusResponse = mutableStateOf<OrderStatusResponse?>(null)


    //add details screen
    val coverAmountList: SnapshotStateList<ProductCoverAmountResponse.Data.ProductCoverAmountsItem?> =
        mutableStateListOf()
    val custId = mutableStateOf("")
    val firstName = mutableStateOf("")
    val lastName = mutableStateOf("")
    val dob = mutableStateOf("")
    val gender = mutableStateOf(Gender.FEMALE)
    val pincode = mutableStateOf("")
    val address = mutableStateOf("")
    val district = mutableStateOf("")
    val state = mutableStateOf("")
    val emailId = mutableStateOf("")
    val phoneNo = mutableStateOf("")
    val whatAppConsent = mutableStateOf(false)

    //transaction status screen
    val orderPdfLink = mutableStateOf("")


    fun productCoverAmount() {
        viewModelScope.launch {
            HospicashRequiredData.access_token?.let { token ->
                loading.value = true
                repository.productCoverAmount(token = token).handleFlow(onLoading = {
                    loading.value = it
                }, onSuccess = {
                    if (it.status == 0) {
                        coverAmountList.clear()
                        it.data?.productCoverAmounts?.let { coverAmountItem ->
                            coverAmountList.addAll(coverAmountItem)
                        }
                    } else {
                        failureMessage.value = it.message ?: "Unable to process, Please try again"
                        failureStatus.value = true
                    }
                }, onFailure = { msg, obj, _ ->
                    failureMessage.value = obj.getString("message") ?: msg
                    failureStatus.value = true
                })
            }
        }
    }

    fun productDetails(coverAmount: Int, onSuccess: (ProductDetailsResponse) -> Unit) {
        viewModelScope.launch {
            loading.value = true

            HospicashRequiredData.access_token?.let { token ->
                repository.productDetails(
                    token = token, body = ProductDetailsRequest(
                        sumInsured = listOf(coverAmount)
                    )
                ).handleFlow(onLoading = {
                    loading.value = it
                }, onSuccess = {
                    if (it.status?.equals(0) == true && it.data?.isNotEmpty() == true) {
                        productDetails.value = null
                        productDetails.value = it
                        onSuccess(it)
                    } else {
                        failureMessage.value = it.message ?: "Unable to process, Please try again"
                        failureStatus.value = true
                    }
                }, onFailure = { msg, obj, _ ->
                    failureMessage.value = obj.getString("message") ?: msg
                    failureStatus.value = true
                })
            }
        }
    }

    fun createOrder(onSuccess: (CreateOrderResponse) -> Unit) {
        viewModelScope.launch {
            loading.value = true

            HospicashRequiredData.access_token?.let { token ->
                val body = CreateOrderRequest(
                    watsAppConsent = whatAppConsent.value, memberList = listOf(
                        CreateOrderRequest.MemberListItem(
                            insuredMobile = phoneNo.value,
                            gender = gender.value.name,
                            insuredEmail = emailId.value,
                            memberName = "${firstName.value} ${lastName.value}",
                            dateOfBirth = dob.value,
                            relation = "self",
                        )
                    ), customer = CreateOrderRequest.Customer(
                        firstName = firstName.value,
                        lastName = lastName.value,
                        pincode = pincode.value,
                        address = "${address.value}, ${district.value}, ${state.value}",
                        custdateOfBirth = dob.value,
                        mobile = phoneNo.value,
                        gender = gender.value.name,
                        emailId = emailId.value,
                    )
                )
                repository.createOrder(token, body).handleFlow(onLoading = {
                    loading.value = it
                }, onFailure = { msg, obj, _ ->
                    failureMessage.value = obj.getString("message") ?: msg
                    failureStatus.value = true
                }, onSuccess = {
                    if (it.status?.equals(0) == true && it.data != null) {
                        onSuccess(it)
                    } else {
                        failureMessage.value = it.message ?: "Unable to process, Please try again"
                        failureStatus.value = true
                    }
                })
            }
        }
    }

    fun orderStatus(orderRefNo: String, onSuccess: (OrderStatusResponse) -> Unit) {
        viewModelScope.launch {
            loading.value = true
            HospicashRequiredData.access_token?.let { token ->
                val body = OrderStatusRequest(
                    orderRefNo = orderRefNo
                )
                repository.orderStatus(token, body).handleFlow(onLoading = {
                    loading.value = it
                }, onFailure = { msg, obj, _ ->
                    failureMessage.value = obj.getString("message") ?: msg
                    failureStatus.value = true
                }, onSuccess = {
                    if (it.status?.equals(0) == true && it.data != null) {
                        orderStatusResponse.value = null
                        orderStatusResponse.value = it
                        onSuccess(it)
                    } else {
                        failureMessage.value = it.message ?: "Unable to process, Please try again"
                        failureStatus.value = true
                    }
                })
            }
        }
    }

    fun pincodeFetch(pincode: String) {
        viewModelScope.launch {
//            loading.value = true
            HospicashRequiredData.access_token?.let { token ->
                val body = PincodeFetchRequest(
                    pincode = pincode
                )
                repository.pincodeFetch(token, body).handleFlow(onLoading = {
//                    loading.value = it
                }, onFailure = { msg, obj, _ ->
                    failureMessage.value = obj.getString("message") ?: msg
                    failureStatus.value = true
                }, onSuccess = {
                    if (it.status?.equals(0) == true && it.data != null) {
                        district.value = it.data.city ?: ""
                        state.value = it.data.stateName ?: ""
                    } else {
                        failureMessage.value = it.message ?: "Unable to fetch pincode, Please try again"
                        failureStatus.value = true
                    }
                })
            }
        }
    }

    fun orderPDF(orderRefNo: String,onSuccess: (String) -> Unit) {
        viewModelScope.launch {
//            loading.value = true
            HospicashRequiredData.access_token?.let { token ->
                val body = OrderStatusRequest(
                    orderRefNo = orderRefNo
                )
                repository.orderPDF(token, body).handleFlow(onLoading = {
//                    loading.value = it
                }, onFailure = { msg, obj, _ ->
                    failureMessage.value = obj.getString("message") ?: msg
                    failureStatus.value = true
                }, onSuccess = {
                    if (it.status?.equals(0) == true && it.data != null) {
                        orderPdfLink.value = it.data?:""
                        onSuccess(orderPdfLink.value)
                    } else {
                        failureMessage.value = it.message ?: "Unable to genrate pdf, Please try again"
                        failureStatus.value = true
                    }
                })
            }
        }
    }

    @SuppressLint("SuspiciousIndentation")
    fun sendOTP(
        email: String, mobile: String, onSuccess: (OtpResponseBody) -> Unit, onFailure: () -> Unit
    ) {
        viewModelScope.launch {
            loading.value = true
            HospicashRequiredData.access_token?.let { token ->
                try {
                    repository.sendOtp(
                        token, SendOtpRequestBody(
                            email = email, mobileNo = mobile
                        )
                    ).handleFlow(onLoading = {
                        loading.value = it
                    }, onSuccess = {
                        if (it.status == 0) {
                            onSuccess(it)
                        } else {
                            onFailure.invoke()
                            failureMessage.value = it.message
                            failureStatus.value = true
                        }
                    }, onFailure = { msg, obj, _ ->
                        onFailure.invoke()
                        failureMessage.value = obj.getString("message") ?: msg
                        failureStatus.value = true
                    })
                } catch (_: Exception) {
                    onFailure.invoke()
                }
            }
        }
    }


    fun verifyOtp(
        otp: String, mobile: String, onSuccess: (OtpResponseBody) -> Unit, onFailure: () -> Unit
    ) {
        viewModelScope.launch {
            loading.value = true
            HospicashRequiredData.access_token?.let { token ->
                try {
                    repository.ValidateOtp(
                        token, ValidateOtpRequestBody(
                            mobileNo = mobile, otp = otp
                        )
                    ).handleFlow(onLoading = {
                        loading.value = it
                    }, onSuccess = {
                        if (it.status == 0) {
                            onSuccess(it)
                        } else {
                            onFailure.invoke()
                            failureMessage.value = it.message
                            failureStatus.value = true
                        }
                    }, onFailure = { msg, obj, _ ->
                        failureMessage.value = obj.getString("message") ?: msg
                        onFailure.invoke()
                        failureStatus.value = true
                    })
                } catch (_: Exception) {
//do nothing
                }
            }
        }
    }

    fun reSendOTP(
        email: String, mobile: String, onSuccess: (OtpResponseBody) -> Unit, onFailure: () -> Unit
    ) {
        viewModelScope.launch {
            loading.value = true
            HospicashRequiredData.access_token?.let { token ->
                try {
                    repository.sendOtp(
                        token, SendOtpRequestBody(
                            email = email, mobileNo = mobile
                        )
                    ).handleFlow(onLoading = {
                        loading.value = it
                    }, onSuccess = {
                        if (it.status == 0) {
                            onSuccess(it)
                        } else {
                            onFailure()
                            failureMessage.value = it.message
                            failureStatus.value = true
                        }
                    }, onFailure = { msg, obj, _ ->
                        onFailure.invoke()
                        failureMessage.value = obj.getString("message") ?: msg
                        failureStatus.value = true
                    })
                } catch (_: Exception) {
                    onFailure.invoke()
                }
            }
        }
    }
}

