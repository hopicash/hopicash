package com.isu.hospicashinapp.presenter

import android.annotation.SuppressLint
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.background
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.isu.hospicashinapp.R

@SuppressLint("SuspiciousIndentation")
@Preview
@Composable
fun TermsAndConditionDialog(
    onCancel: () -> Unit = {}
) {
    val isChecked = remember {
        mutableStateOf(false)
    }
    val context = LocalContext.current


            Dialog(
                onDismissRequest = onCancel,
                properties = DialogProperties(
                    dismissOnBackPress = true,
                    dismissOnClickOutside = true
                )

            ) {
                Card(
                    modifier = Modifier.wrapContentSize(),
                    shape = RoundedCornerShape(8.dp),
                    colors = CardDefaults.cardColors(Color.White),
                    elevation = CardDefaults.cardElevation(defaultElevation = 4.dp)
                ) {

                    Box(
                        modifier = Modifier
                            .wrapContentSize()
                            .padding(top = 10.dp)
                    ) {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(24.dp), contentAlignment = Alignment.TopEnd
                        ) {
                            IconButton(onClick = {
                                onCancel.invoke()
                            }) {
                                Icon(
                                    painter = painterResource(id = R.drawable.baseline_clear_24),
                                    "", modifier = Modifier
                                        .height(24.dp)
                                        .width(24.dp)
                                        .background(color = Color.Transparent)
                                        .padding(0.dp)
                                )
                            }
                        }
                        Column {
                            Row(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .height(24.dp),
                                verticalAlignment = Alignment.CenterVertically,
                                horizontalArrangement = Arrangement.Center
                            ) {
                                Text(
                                    "Terms & Condition",
                                    fontSize = 18.sp,
                                    color = Color(0xFF333333),
                                    fontWeight = FontWeight(600)
                                )
                            }
                            Spacer(modifier = Modifier.height(26.dp))
                            Column(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .wrapContentHeight().verticalScroll(ScrollState(1))
                                    .padding(
                                        start = 12.dp,
                                        end = 12.dp,
                                        top = 24.dp,
                                        bottom = 24.dp
                                    )
                            ) {
                                Text(color = Color.Black,fontWeight = FontWeight(500), fontSize = 14.sp,text = "I here by declare that all proposed members are in good health and entirely free from any mental or physical impairments or deformalities, disease/condition. Also none of the proposed members are habitual consumer of alchol, tabaco, gutka or any recreational drugs.\n" +
                                        "\n" +
                                        "I agree that the company and its representatives can contact me through call,SMS, Whatsapp or email even if i am registred under NDNC.\n" +
                                        "\n" +
                                        "I also agree that i have read and understood that standrad T&C, privacy & disclaimer and agree to abide by the same.")
                            }





                            Row(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(
                                        start = 15.dp,
                                        end = 10.dp,
                                        top = 5.dp,
                                        bottom = 15.dp,

                                        ), horizontalArrangement = Arrangement.SpaceEvenly
                            ) {

                                Button(
                                    onClick = {
                                              onCancel.invoke()
                                    },
                                    shape = RoundedCornerShape(8.dp),
                                    modifier = Modifier
                                        .height(46.dp)
                                        .width(149.dp)
                                        .weight(1f)
                                        .padding(end = 10.dp),
                                    colors = ButtonDefaults.buttonColors(
                                        containerColor = Color(0xFFD2D2D2)
                                    )

                                ) {
                                    Text(
                                        text = "Cancel",
                                        fontWeight = FontWeight(600),
                                        fontSize = 16.sp,
                                        color = Color(0xFF333333)
                                    )
                                }

                            }
                        }

                    }


                }

            }





}



