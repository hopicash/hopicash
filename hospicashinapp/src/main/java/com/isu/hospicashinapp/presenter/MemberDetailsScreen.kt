package com.isu.hospicashinapp.presenter

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withAnnotation
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.isu.hospicashinapp.HospicashViewModel
import com.isu.hospicashinapp.R


@OptIn(ExperimentalTextApi::class)
@Composable
fun MemberDetailsScreen(navController: NavHostController, viewModel: HospicashViewModel) {


    val isChecked = remember {
        mutableStateOf(false)
    }
    val isTermsDialog = remember {
        mutableStateOf(false)
    }
    val context = LocalContext.current

    Scaffold(modifier = Modifier.fillMaxSize(), topBar = {
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .height(60.dp),
            shape = RectangleShape,
            elevation = CardDefaults.cardElevation(10.dp),
            colors = CardDefaults.cardColors(Color.White)

        ) {
            Box(modifier = Modifier.fillMaxSize()) {
                Box(
                    modifier = Modifier.fillMaxSize(),
                    contentAlignment = Alignment.CenterStart,
                ) {
                    IconButton(onClick = {
                        //write the code for navigation here
                        navController.popBackStack()

                    }) {
                        Icon(
                            painter = painterResource(id = R.drawable.baseline_arrow_back_24),
                            "",
                            modifier = Modifier
                                .height(24.dp)
                                .width(24.dp)
                                .background(color = Color.Transparent)
                                .padding(0.dp)
                        )
                    }
                }
                //write the code for text "member details" here

                Box(
                    modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center
                ) {
                    Text(
                        "Member Details",
                        fontSize = 18.sp,
                        fontWeight = FontWeight.Bold
                    )
                }
            }

        }
    }) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(it)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(24.dp)
                    .verticalScroll(rememberScrollState(0), true),
                verticalArrangement = Arrangement.SpaceBetween
            ) {
                Box {
                    Column {
                        DetailsRow("Customer I'd", viewModel.custId.value)
                        DetailsRow("First Name", viewModel.firstName.value)
                        DetailsRow("Last Name", viewModel.lastName.value)
                        DetailsRow("Phone Number", viewModel.phoneNo.value)
                        DetailsRow("Email I'd", viewModel.emailId.value)
                        DetailsRow("DOB", viewModel.dob.value)
                        DetailsRow("Gender", viewModel.gender.value.name)
                        DetailsRow("Address", viewModel.address.value)
                        DetailsRow("PIN", viewModel.pincode.value)
                        DetailsRow("District", viewModel.district.value)
                        DetailsRow("State", viewModel.state.value)



                        Row(
                            modifier = Modifier
                                .wrapContentSize()
                                .padding(
                                    start = 0.dp, end = 5.dp, top = 25.dp, bottom = 5.dp
                                ),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Checkbox(
                                checked = isChecked.value, onCheckedChange = { che ->
                                    isChecked.value = che
                                }, modifier = Modifier
                                    .width(50.dp)
                                    .offset(y = (-10).dp)
                            )
                            val annotatedString = buildAnnotatedString {
                                withStyle(
                                    style = SpanStyle(
                                        fontSize = 13.sp, fontWeight = FontWeight.SemiBold
                                    )
                                ) {
                                    append("I Declare that, I am of good health and agree with")
                                }

                                withStyle(
                                    style = SpanStyle(
                                        fontSize = 14.sp,
                                        fontWeight = FontWeight.Bold,
                                        color = MaterialTheme.colorScheme.primary
                                    )
                                ) {
                                    withAnnotation("terms", "Terms and Condition") {
                                        append("  Terms and Condition")
                                    }
                                }

                            }

                            ClickableText(text = annotatedString, onClick = { offset ->
                                annotatedString.getStringAnnotations(offset, offset)
                                    .firstOrNull()?.tag?.let { str ->
                                        if (str.equals("terms", true)) {
                                            isTermsDialog.value = true
                                        }
                                    }
                            })
                        }
                    }
                }



                Row(
                    modifier = Modifier
                        .wrapContentSize()
                        .padding(
                            start = 15.dp,
                            end = 10.dp,
                            top = 5.dp,
                            bottom = 15.dp,

                            ), horizontalArrangement = Arrangement.SpaceEvenly
                ) {

                    Button(
                        onClick = {
                            navController.popBackStack()
                        },
                        shape = RoundedCornerShape(8.dp),
                        modifier = Modifier
                            .height(46.dp)
                            .width(149.dp)
                            .weight(1f)
                            .padding(end = 10.dp),
                        colors = ButtonDefaults.buttonColors(
                            containerColor = Color(0xFFD2D2D2)
                        )

                    ) {
                        Text(
                            text = "Cancel",
                            fontWeight = FontWeight(600),
                            fontSize = 16.sp,
                        )
                    }
                    Button(
                        onClick = {

                            if (isChecked.value) {
                                viewModel.createOrder { res ->
                                    Toast.makeText(
                                        context,
                                        "Your order has been create successfully,",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    res.data?.orderRefNumber?.let { refNo ->
                                        viewModel.orderStatus(
                                            orderRefNo = refNo
                                        ) {
                                            navController.navigate(Screen.STATUS_SCREEN.name)
                                        }
                                    }
                                }
                            } else {
                                Toast.makeText(
                                    context,
                                    "Please read the terms and condition",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        },
                        shape = RoundedCornerShape(8.dp),
                        modifier = Modifier
                            .height(46.dp)
                            .width(149.dp)
                            .weight(1f)
                            .padding(end = 10.dp)

                    ) {
                        Text(
                            text = "Pay Now",
                            fontWeight = FontWeight(600),
                            fontSize = 16.sp,
                            color = Color.White
                        )

                    }
                }
            }


        }
        if (isTermsDialog.value) {
            TermsAndConditionDialog {
                isTermsDialog.value = false
            }
        }
    }
}

@Composable
fun DetailsRow(s: String, s1: String) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                top = 6.dp, bottom = 6.dp
            ),
        horizontalArrangement = Arrangement.spacedBy(20.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = s,
            fontWeight = FontWeight(400),
            modifier = Modifier
                .weight(1f)
                .padding(end = 5.dp),
            textAlign = TextAlign.Start
        )

        Text(
            modifier = Modifier.weight(1f),
            text = s1,
            fontWeight = FontWeight(500),
            textAlign = TextAlign.Start
        )
    }
}



