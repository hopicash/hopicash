package com.isu.hospicashinapp.presenter

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonColors
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp

@Composable
fun IconCustomButton( modifier: Modifier, res:Int, onClick:()-> Unit) {
    IconButton(onClick = {onClick()},

        modifier = modifier.wrapContentSize().clip(CircleShape).size(34.dp).background(color = Color(0xFFF2F2F2), shape = CircleShape)
                ,) {

            Icon( painter = painterResource(id = res),"", tint = Color(0xFF0ABCD0),modifier = Modifier.height(20.dp).width(18.dp).clip(
                CircleShape))




    }
}