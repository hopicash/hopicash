package com.isu.hospicashinapp.presenter

import android.app.Activity
import android.widget.Toast
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.BottomSheetScaffold
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SheetState
import androidx.compose.material3.SheetValue
import androidx.compose.material3.Text
import androidx.compose.material3.rememberBottomSheetScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.isu.hospicashinapp.HospicashViewModel
import com.isu.hospicashinapp.R
import com.isu.hospicashinapp.data.model.res.ProductCoverAmountResponse

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PlanScreen(navController: NavHostController, viewModel: HospicashViewModel) {
    val context = LocalContext.current
    val activity = LocalContext.current as Activity
    val activateBottomSheet = rememberSaveable { mutableStateOf(false) }
    val sheetState = rememberBottomSheetScaffoldState(
        bottomSheetState = SheetState(
            initialValue = SheetValue.Hidden, skipPartiallyExpanded = true
        )
    )

    LaunchedEffect(key1 = activateBottomSheet.value) {
        if (activateBottomSheet.value) {
            sheetState.bottomSheetState.expand()
        } else {
            sheetState.bottomSheetState.hide()
        }
    }

//    LaunchedEffect(key1 = sheetState.bottomSheetState.currentValue.name) {
////        if () {
////            Toast.makeText(context, "visible", Toast.LENGTH_SHORT).show()
////        } else {
////            Toast.makeText(context, "${sheetState.bottomSheetState.currentValue.name}", Toast.LENGTH_SHORT).show()
//
////        }
//        if (sheetState.bottomSheetState.currentValue.name.equals("Hidden")){
//            activateBottomSheet.value = false
//        }
//    }



    LaunchedEffect(key1 = Unit) {
        viewModel.productCoverAmount()
    }



    BottomSheetScaffold(modifier = Modifier
        .fillMaxSize()
        .padding(0.dp),
        scaffoldState = sheetState,
        sheetSwipeEnabled = false,
        sheetPeekHeight = if (activateBottomSheet.value) 60.dp else 0.dp,
        sheetContent = {
            BottomSheetContent(activateBottomSheet, navController, viewModel)
        }) {
        Column(Modifier.fillMaxSize()) {
            Row(
                Modifier
                    .fillMaxWidth()
                    .shadow(elevation = 2.dp)
                    .height(55.dp),

                verticalAlignment = CenterVertically
            ) {

                Icon(Icons.Default.ArrowBack,
                    contentDescription = null,
                    modifier = Modifier.clickable {
                        activity.finish()
                    })
                Text(
                    text = "Select Plan",
                    fontSize = 18.sp,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.weight(1f)
                )
            }

//            Column(
//                Modifier
//                    .verticalScroll(scrollState)
//                    .fillMaxSize()
//                    .weight(1f)
//                    .padding(16.dp)
//            ) {
//                PlanItem(activateBottomSheet,navController)
//                PlanItem(activateBottomSheet,navController)
//            }

            LazyColumn(
                Modifier
                    .fillMaxSize()
                    .weight(1f)
                    .padding(16.dp)
            ) {
                items(viewModel.coverAmountList) {
                    it?.let {
                        PlanItem(activateBottomSheet, navController, it, viewModel)
                    }
                }
            }
        }
    }
}

@Composable
fun PlanItem(
    activateBottomSheet: MutableState<Boolean>,
    navController: NavHostController,
    productCoverAmountsItem: ProductCoverAmountResponse.Data.ProductCoverAmountsItem,
    viewModel: HospicashViewModel
) {
    Column(
        Modifier
            .padding(bottom = 10.dp)
            .fillMaxWidth()
            .shadow(2.dp, RoundedCornerShape(10.dp))
            .background(MaterialTheme.colorScheme.background, RoundedCornerShape(10.dp))
            .padding(12.dp)
    ) {
        Row(Modifier.fillMaxWidth()) {
            Image(
                painter = painterResource(id = R.drawable.frame44),
                contentDescription = null,
                modifier = Modifier.size(50.dp)
            )
            Spacer(modifier = Modifier.width(10.dp))
            Column(Modifier.weight(1f)) {
                Text(text = "Cover of", fontSize = 16.sp)
                Text(
                    text = "₹ ${productCoverAmountsItem.coverAmount}",
                    fontSize = 16.sp,
                    fontWeight = FontWeight.SemiBold
                )
            }

            Button(
                modifier = Modifier.weight(1f),
                onClick = {
//                    navController.navigate(Screen.ADD_DETAIL_SCREEN.name)
                    viewModel.productDetails(productCoverAmountsItem.coverAmount ?: 0) {
                        activateBottomSheet.value = !activateBottomSheet.value
                    }
                },
                border = BorderStroke(1.dp, MaterialTheme.colorScheme.primary),
                shape = RoundedCornerShape(32.dp),
                colors = ButtonDefaults.buttonColors(
                    containerColor = MaterialTheme.colorScheme.background
                )
            ) {
                Text(
                    text = "Buy Now ₹ ${productCoverAmountsItem.coverAmtExt}",
                    color = MaterialTheme.colorScheme.primary
                )
            }
        }
        Spacer(modifier = Modifier.height(10.dp))
        Text(text = "View Policy Details",
            color = MaterialTheme.colorScheme.primary,
            modifier = Modifier
                .padding(start = 60.dp)
                .clickable {
//                    if (!activateBottomSheet.value){
                    viewModel.productDetails(productCoverAmountsItem.coverAmount ?: 0) {
                        activateBottomSheet.value = !activateBottomSheet.value
                    }
//                    }
                })
    }
}

@Composable
fun BottomSheetContent(
    activateBottomSheet: MutableState<Boolean>,
    navController: NavHostController,
    viewModel: HospicashViewModel
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(0.6f)
            .padding(16.dp),
    ) {
        Icon(Icons.Default.Close,
            contentDescription = null,
            modifier = Modifier
                .align(Alignment.TopEnd)
                .clickable {
                    activateBottomSheet.value = false
                }
//                .size(30.dp)
        )

        Column {
            Text(
                text = "Benefits",
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.SemiBold,
                modifier = Modifier.fillMaxWidth(),
                fontSize = 18.sp
            )
            Column(
                Modifier
                    .weight(1f)
                    .padding(10.dp)
            ) {
                Row(Modifier.fillMaxWidth()) {
                    Text(text = "Features", Modifier.weight(1f), fontWeight = FontWeight.SemiBold)
                    Text(text = "Coverage", Modifier.weight(1f), fontWeight = FontWeight.SemiBold)
                }

                if (viewModel.productDetails.value?.data?.isNullOrEmpty() == false) {
                    Column(
                        Modifier
                            .fillMaxWidth()
                            .verticalScroll(rememberScrollState())
                    ) {
                        DetailsRow(
                            "Product Name",
                            viewModel.productDetails.value?.data?.get(0)?.productName ?: "NA"
                        )
                        DetailsRow(
                            "Product Type",
                            viewModel.productDetails.value?.data?.get(0)?.productType ?: "NA"
                        )
                        DetailsRow(
                            "Sum Insured",
                            "${viewModel.productDetails.value?.data?.get(0)?.sumInsured}"
                        )
                        DetailsRow(
                            "Final Premium",
                            "${viewModel.productDetails.value?.data?.get(0)?.finalPremium}"
                        )
                        DetailsRow(
                            "Policy Tenure",
                            "${viewModel.productDetails.value?.data?.get(0)?.policyTenure}"
                        )
                        DetailsRow(
                            "Adult Min Age",
                            "${viewModel.productDetails.value?.data?.get(0)?.adultMinAge}"
                        )
                        DetailsRow(
                            "Adult Max Age",
                            "${viewModel.productDetails.value?.data?.get(0)?.adultMaxAge}"
                        )
                        DetailsRow(
                            "Child Min Age",
                            "${viewModel.productDetails.value?.data?.get(0)?.childMinAge}"
                        )
                        DetailsRow(
                            "Child Max Age",
                            "${viewModel.productDetails.value?.data?.get(0)?.childMaxAge}"
                        )
                        DetailsRow(
                            "No of Hospital",
                            "${viewModel.productDetails.value?.data?.get(0)?.numberOfHospitals}"
                        )
                        DetailsRow("Policy Benefit", "")
                        viewModel.productDetails.value?.data?.get(0)?.shortDescriptionArray?.forEach {
                            DetailsRow(s = "", s1 = it?.value ?: "")
                        }

                    }

                }
            }
            Row(Modifier.height(46.dp)) {
                Button(
                    onClick = { activateBottomSheet.value = false },
                    modifier = Modifier.weight(1f),
                    colors = ButtonDefaults.buttonColors(
                        containerColor = Color.LightGray
                    ),
                    shape = RoundedCornerShape(8.dp)
                ) {
                    Text(
                        text = "Cancel",
                        Modifier
                            .fillMaxWidth()
                            .align(CenterVertically),
                        textAlign = TextAlign.Center,
                        color = Color.Black
                    )
                }

                Spacer(modifier = Modifier.width(16.dp))
                Button(
                    onClick = {
                        navController.navigate(Screen.ADD_DETAIL_SCREEN.name)
                    }, modifier = Modifier.weight(1f), colors = ButtonDefaults.buttonColors(
                        containerColor = MaterialTheme.colorScheme.primary
                    ), shape = RoundedCornerShape(8.dp)
                ) {
                    Text(
                        text = "Proceed",
                        Modifier
                            .fillMaxWidth()
                            .align(CenterVertically),
                        textAlign = TextAlign.Center,
                        color = Color.White
                    )
                }
            }
        }
    }
}