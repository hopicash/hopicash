package com.isu.hospicashinapp.presenter


import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.CountDownTimer
import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalClipboardManager
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.ImageLoader
import coil.compose.rememberAsyncImagePainter
import coil.decode.GifDecoder
import coil.decode.ImageDecoderDecoder
import coil.request.ImageRequest
import coil.size.Size
import com.isu.hospicashinapp.HospicashViewModel
import com.isu.hospicashinapp.R
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


@Composable
fun TransactionStatusScreen(viewModel: HospicashViewModel) {
    val transactionState = remember {
        mutableStateOf(viewModel.orderStatusResponse.value?.data?.orderStatus.equals("Success"))
    }
    val launcher =
        rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) { _ ->
            // Handle the result if needed
        }

    val transactionMessage =
        if (transactionState.value) "Transaction Successful" else "Transaction Failed"
    val transactionColor = if (transactionState.value) Color(0xFF00955D) else Color(0xFFC41425)
    BackHandler {

    }
    Scaffold {
        Column(
            modifier = Modifier
                .padding(it)
                .fillMaxSize()
//                    .verticalScroll(ScrollState(1), true),
        ) {
            GifAnimation(transactionState.value)
            Column(
                Modifier
                    .fillMaxWidth()
                    .padding(start = 20.dp, end = 20.dp, bottom = 24.dp),
                verticalArrangement = Arrangement.spacedBy(5.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    "₹3499.00",
                    fontSize = 25.sp,
                    fontWeight = FontWeight.Bold,
                    color = transactionColor
                )

                Text(
                    text = transactionMessage,
                    fontSize = 25.sp,
                    fontWeight = FontWeight.Bold,
                    color = transactionColor

                )
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(26.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center
                ) {
                    Text(
                        text = getCurrentDateFormatted(),
                        fontSize = 14.sp,
                        fontWeight = FontWeight.SemiBold
                    )
                }

            }


            Column(
                Modifier
                    .padding(start = 38.dp, end = 38.dp)
                    .fillMaxWidth(),
                verticalArrangement = Arrangement.spacedBy(20.dp)
            ) {

                Column(modifier = Modifier.height(48.dp)) {

                    Text("Policy Number", fontWeight = FontWeight.SemiBold, fontSize = 14.sp)

                    Spacer(modifier = Modifier.height(5.dp))

                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        Text(
                            viewModel.orderStatusResponse.value?.data?.orderNo ?: "NA",
                            color = Color.LightGray,
                            fontWeight = FontWeight.SemiBold
                        )
                        CopyToClip(
                            viewModel.orderStatusResponse.value?.data?.orderNo ?: "NA"
                        )
                    }
                }

                Column(modifier = Modifier.height(48.dp)) {
                    Text("Transaction ID", fontWeight = FontWeight.SemiBold, fontSize = 14.sp)
                    Spacer(modifier = Modifier.height(5.dp))
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        Text(
                            text = viewModel.orderStatusResponse.value?.data?.paymentRefNumber
                                ?: "NA", color = Color.LightGray, fontWeight = FontWeight.SemiBold
                        )
                        CopyToClip(
                            viewModel.orderStatusResponse.value?.data?.paymentRefNumber ?: "NA"
                        )
                    }
                }


                Column {
                    Text("Date", fontWeight = FontWeight.SemiBold, fontSize = 14.sp)
                    Spacer(modifier = Modifier.height(5.dp))
                    Text(
                        getCurrentDateFormatted(),
                        color = Color.LightGray,
                        fontWeight = FontWeight.SemiBold
                    )


                }
                Column {
                    Text("Amount", fontWeight = FontWeight.SemiBold, fontSize = 14.sp)
                    Spacer(modifier = Modifier.height(5.dp))
                    Text(
                        "₹${viewModel.orderStatusResponse.value?.data?.amount ?: "NA"}",
                        color = Color.LightGray,
                        fontWeight = FontWeight.SemiBold
                    )
                }

                Column {
                    Text("Mobile Number", fontWeight = FontWeight.SemiBold, fontSize = 14.sp)
                    Spacer(modifier = Modifier.height(5.dp))
                    Text(
                        viewModel.orderStatusResponse.value?.data?.phone ?: "NA",
                        color = Color.LightGray,
                        fontWeight = FontWeight.SemiBold
                    )
                }

                Spacer(modifier = Modifier.weight(1f))

                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.spacedBy(10.dp)
                ) {
                    Spacer(modifier = Modifier.weight(1f))
                    IconCustomButton(
                        res = R.drawable.baseline_share_24, modifier = Modifier.weight(1f)
                    ) {
                        if (viewModel.orderPdfLink.value.isNotBlank()) {
                            val textToShare = """
                                Please Follow the link to download the order pdf
                                
                                ${viewModel.orderPdfLink.value}
                                
                                Thank You
                            """.trimIndent()
                            val sendIntent = Intent().apply {
                                action = Intent.ACTION_SEND
                                putExtra(Intent.EXTRA_TEXT, textToShare)
                                type = "text/plain"
                            }
                            val shareIntent = Intent.createChooser(sendIntent, null)
                            launcher.launch(shareIntent)
                        } else {
                            viewModel.orderPDF(
                                viewModel.orderStatusResponse.value?.data?.orderRefNumber ?: ""
                            ) { link ->
                                val textToShare = """
                                Please Follow the link to download the order pdf
                                
                                $link
                                
                                Thank You
                            """.trimIndent()
                                val sendIntent = Intent().apply {
                                    action = Intent.ACTION_SEND
                                    putExtra(Intent.EXTRA_TEXT, textToShare)
                                    type = "text/plain"
                                }
                                val shareIntent = Intent.createChooser(sendIntent, null)
                                launcher.launch(shareIntent)
                            }
                        }
                    }
                    IconCustomButton(
                        modifier = Modifier.weight(1f), res = R.drawable.baseline_print_24
                    ) {
                        if (viewModel.orderPdfLink.value.isNotBlank()) {
                            val intent =
                                Intent(Intent.ACTION_VIEW, Uri.parse(viewModel.orderPdfLink.value))
                            launcher.launch(intent)
                        } else {
                            viewModel.orderPDF(
                                viewModel.orderStatusResponse.value?.data?.orderRefNumber ?: ""
                            ) {
                                val intent = Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse(viewModel.orderPdfLink.value)
                                )
                                launcher.launch(intent)
                            }
                        }
                    }
                    IconCustomButton(
                        res = R.drawable.baseline_file_download_24, modifier = Modifier.weight(1f)
                    ) {
                        if (viewModel.orderPdfLink.value.isNotBlank()) {
                            val intent =
                                Intent(Intent.ACTION_VIEW, Uri.parse(viewModel.orderPdfLink.value))
                            launcher.launch(intent)
                        } else {
                            viewModel.orderPDF(
                                viewModel.orderStatusResponse.value?.data?.orderRefNumber ?: ""
                            ) {
                                val intent = Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse(viewModel.orderPdfLink.value)
                                )
                                launcher.launch(intent)
                            }
                        }
                    }
                    Spacer(modifier = Modifier.weight(1f))
                }


                val activity = LocalContext.current as Activity
                Button(
                    onClick = {
                        activity.finishAffinity()
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(43.dp),
                    shape = RoundedCornerShape(8.dp)
                ) {
                    Text("Finish", fontWeight = FontWeight.SemiBold, fontSize = 16.sp)
                }
                Spacer(modifier = Modifier.height(24.dp))
            }
        }

    }
}

@Composable
fun GifAnimation(value: Boolean) {
    val statusAnimation = if (value) R.raw.successhd else R.raw.failedhd
    val imageLoader = ImageLoader.Builder(LocalContext.current).components {
        if (Build.VERSION.SDK_INT >= 28) {
            add(ImageDecoderDecoder.Factory())
        } else {
            add(GifDecoder.Factory())
        }
    }.build()
    val painter = rememberAsyncImagePainter(
        ImageRequest.Builder(LocalContext.current).data(data = statusAnimation).apply(block = {
            size(Size(800, 800))
        }).build(), imageLoader = imageLoader
    )
    var finished by rememberSaveable {
        mutableStateOf(false)
    }

    LaunchedEffect(key1 = true) {
        object : CountDownTimer(1000000000, 100000000) {
            override fun onTick(millisUntilFinished: Long) {
                //do nothing
            }

            override fun onFinish() {
                finished = true
            }
        }.start()
    }
    Box(
        modifier = Modifier
            .height(150.dp)
            .fillMaxWidth()
    ) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                modifier = Modifier
                    .padding(top = 50.dp)
                    .scale(2f)
                    .background(color = Color.Transparent),
                painter = painter,
                contentDescription = null
            )
        }

    }

}

@Composable
fun CopyToClip(clipText: String) {
    val clipBoard = LocalClipboardManager.current
    val context = LocalContext.current
    IconButton(onClick = {
        clipBoard.setText(AnnotatedString(clipText))
        Toast.makeText(context, "Copied..", Toast.LENGTH_SHORT).show()
    }, modifier = Modifier.offset(y = (-15).dp)) {
        Icon(
            tint = Color(0xFF0ABCD0),
            painter = painterResource(R.drawable.baseline_content_copy_24),
            contentDescription = ""
        )
    }

}

fun getCurrentDateFormatted(): String {
    val dateFormat = SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
    val currentDate = Date()
    return dateFormat.format(currentDate)
}