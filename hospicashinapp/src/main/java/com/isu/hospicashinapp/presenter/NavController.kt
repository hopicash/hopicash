package com.isu.hospicashinapp.presenter

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.isu.hospicashinapp.HospicashViewModel


@Composable
fun HospicashNavigation(navController: NavHostController, viewModel: HospicashViewModel) {
    NavHost(
        navController = navController, startDestination = Screen.PLAN_SCREEN.name
    ) {
        composable(Screen.PLAN_SCREEN.name) { PlanScreen(navController, viewModel) }
        composable(Screen.ADD_DETAIL_SCREEN.name) { AddDetailsScreen(navController, viewModel) }
        composable(Screen.STATUS_SCREEN.name) { TransactionStatusScreen(viewModel) }
        composable(Screen.MEMBER_DETAILS_SCREEN.name){ MemberDetailsScreen(navController = navController, viewModel = viewModel)}
    }
}

enum class Screen {
    PLAN_SCREEN, ADD_DETAIL_SCREEN, STATUS_SCREEN,MEMBER_DETAILS_SCREEN
}
