package com.isu.hospicashinapp.presenter

import android.annotation.SuppressLint
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.navigation.NavHostController
import coil.ImageLoader
import coil.compose.rememberAsyncImagePainter
import coil.decode.SvgDecoder
import com.isu.hospicashinapp.HospicashViewModel
import com.isu.hospicashinapp.R

@SuppressLint("SuspiciousIndentation")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun OtpDialogScreen(
    navController: NavHostController,
    viewModel: HospicashViewModel,
    onCancel: () -> Unit = {}
    //value : manage Value here
) {
    val context = LocalContext.current
    val otpState  = remember{
        mutableStateOf("")
    }
    val state = viewModel.loading



            Dialog(
                onDismissRequest = onCancel,
                properties = DialogProperties(
                    dismissOnBackPress = true,
                    dismissOnClickOutside = true
                )

            ) {
                Card(
                    modifier = Modifier.wrapContentSize(),
                    shape = RoundedCornerShape(8.dp),
                    colors = CardDefaults.cardColors(Color.White),
                    elevation = CardDefaults.cardElevation(defaultElevation = 4.dp)
                ) {

                    Box(
                        modifier = Modifier
                            .wrapContentSize()
                            .padding(top = 20.dp)
                    ) {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(30.dp), contentAlignment = Alignment.TopEnd
                        ) {
                            IconButton(onClick = { onCancel.invoke() }) {
                                Icon(
                                    painter = painterResource(id = R.drawable.baseline_clear_24),
                                    "", modifier = Modifier
                                        .height(24.dp)
                                        .width(24.dp)
                                        .background(color = Color.Transparent)
                                        .padding(0.dp)
                                )
                            }
                        }

                        Column(
                            modifier = Modifier
                                .wrapContentSize()
                                .padding(start = 20.dp, end = 20.dp, bottom = 20.dp)
                                .verticalScroll(ScrollState(1), true),
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Box(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .height(150.dp),
                                contentAlignment = Alignment.Center
                            ) {
                                val imageLoader = ImageLoader.Builder(LocalContext.current)
                                    .components {
                                        add(SvgDecoder.Factory())
                                    }
                                    .build()

                                Image(
                                    painter = rememberAsyncImagePainter(
                                        R.raw.otp_sticker,
                                        imageLoader
                                    ),
                                    contentDescription = null,
                                    modifier = Modifier.fillMaxSize()
                                )
                                // Image(painter = painterResource(id = R.raw.otpsticker), contentDescription = "",modifier = Modifier.scale(2.5f))
                            }
                            Text(
                                modifier = Modifier.padding(top = 6.dp, bottom = 5.dp),
                                text = "Verify Your Mobile Number",
                                color = Color.Black,
                                fontSize = 16.sp,
                                fontWeight = FontWeight(600)
                            )
                            Text(
                                modifier = Modifier.padding(bottom = 10.dp).fillMaxWidth()
                                , textAlign = TextAlign.Center,
                                text = viewModel.phoneNo.value,
                                color = Color(0xFFADADAD),
                                fontWeight = FontWeight(300),
                                fontSize = 12.sp
                            )

                            Box(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .height(70.dp),
                                contentAlignment = Alignment.Center

                            ) {


                                BasicTextField(
                                    value = otpState.value,
                                    onValueChange = {

                                    if (it.length <= 6) {
                                        otpState.value = it
                                    }


                                }, keyboardOptions = KeyboardOptions(
                                    imeAction = ImeAction.Done, keyboardType = KeyboardType.NumberPassword)
                                )

                                {


                                    Row(
                                        horizontalArrangement = Arrangement.SpaceEvenly,
                                        modifier = Modifier.fillMaxWidth()
                                    ) {
                                        repeat(6) {

                                            val char = when {
                                                it >= otpState.value.length -> ""
                                                else -> otpState.value[it]
                                            }


                                            Card(
                                                Modifier
                                                    .padding(1.dp)
                                                    .height(39.dp)
                                                    .width(34.dp),
                                                colors = CardDefaults.cardColors(Color.White),
                                                shape = RoundedCornerShape(5.dp),

                                                elevation = CardDefaults.cardElevation(1.dp)
                                            ) {

                                                Box(
                                                    Modifier.fillMaxSize(), contentAlignment = Alignment.Center
                                                ) {

                                                    Text(
                                                        char.toString(),
                                                        textAlign = TextAlign.Center,
                                                        color = Color.Black,
                                                        fontWeight = FontWeight(500)
                                                    )

                                                }
                                            }

                                        }
                                    }

                                }

                            }
                            Row(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .wrapContentSize(),
                                verticalAlignment = Alignment.CenterVertically,
                                horizontalArrangement = Arrangement.Center
                            ) {
                                Text(
                                    text = "Didn’t receive an otp?",
                                    fontSize = 12.sp,
                                    fontWeight = FontWeight(300),
                                    color = Color.Black
                                )
                                Spacer(modifier = Modifier.width(3.dp))
                                Text(
                                    modifier = Modifier.clickable {
                                        state.value = true
                                        //navController.navigate(Screen.STATUS_SCREEN.name)
                                        if(viewModel.phoneNo.value.length != 10 && viewModel.emailId.value.trim().isNotEmpty()){
                                            Toast.makeText(context,"Please Enter A Valid Number or Email",Toast.LENGTH_LONG).show()
                                        }else{
                                            viewModel.reSendOTP(viewModel.emailId.value,viewModel.phoneNo.value, onSuccess = {
                                                Toast.makeText(context,"OTP sent successfully",Toast.LENGTH_LONG).show()

                                            }, onFailure = {
                                                val error = viewModel.failureMessage.value
                                                Toast.makeText(context, error,Toast.LENGTH_LONG).show()

                                            })

                                        }
                                    },

                                    text = "Resend OTP.",
                                    fontWeight = FontWeight(400),
                                    fontSize = 15.sp,
                                    color = MaterialTheme.colorScheme.primary
                                )

                            }

                            Button(
                                onClick = {
                                    if(otpState.value.length != 6){
                                        Toast.makeText(context, "Please enter a valid OTP", Toast.LENGTH_SHORT).show()
                                    }else{
                                        state.value = true
                                        viewModel.verifyOtp(otpState.value,viewModel.phoneNo.value, onFailure = {
                                            val error = viewModel.failureMessage.value
                                            Toast.makeText(context, error,Toast.LENGTH_LONG).show()
                                        }, onSuccess = {
                                            navController.navigate(Screen.MEMBER_DETAILS_SCREEN.name)
                                            onCancel.invoke()
                                        })

                                    }

                                          },
                                enabled = !state.value,
                                modifier = Modifier
                                    .height(65.dp)
                                    .width(219.dp)
                                    .padding(10.dp),
                                shape = RoundedCornerShape(10.dp)
                            ) {
                                Text(
                                    "Verify",
                                    fontSize = 16.sp,
                                    color = Color.White,
                                    fontWeight = FontWeight(600)
                                )
                            }


                        }

                    }


                }

            }
}
