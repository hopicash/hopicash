package com.isu.hospicashinapp.presenter

import android.annotation.SuppressLint
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.DatePicker
import androidx.compose.material3.DatePickerState
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.isu.hospicashinapp.HospicashViewModel
import com.isu.hospicashinapp.R
import com.isu.hospicashinapp.ui.theme.TransparentBlack
import java.text.SimpleDateFormat
import java.util.Date

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddDetailsScreen(navController: NavHostController, viewModel: HospicashViewModel) {
    val context = LocalContext.current
    val datePickerState = rememberDatePickerState()
    val datePickerDialogState = remember { mutableStateOf(false) }
    val dateSelected = remember { mutableStateOf(false) }

    val showOtpDialog = remember {
        mutableStateOf(false)
    }

    LaunchedEffect(key1 = viewModel.pincode.value) {
        if (viewModel.pincode.value.length == 6) {
            viewModel.pincodeFetch(viewModel.pincode.value)
        }
    }

    Column(Modifier.fillMaxSize()) {
        Row(
            Modifier
                .fillMaxWidth()
                .shadow(elevation = 2.dp)
                .height(55.dp),

            verticalAlignment = Alignment.CenterVertically
        ) {

            Icon(Icons.Default.ArrowBack, contentDescription = null, modifier = Modifier
                .clickable {
                    navController.popBackStack()
                }
                .padding(start = 24.dp))
            Text(
                text = "Add Details",
                fontSize = 18.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier.weight(1f)
            )
        }

        val scrollState = rememberScrollState()


        Column(
            Modifier
                .verticalScroll(scrollState)
                .fillMaxSize()
                .weight(1f)
                .padding(16.dp)
        ) {
            Spacer(modifier = Modifier.height(10.dp))
            OutlinedTextField(value = "", onValueChange = {}, placeholder = {
                Text(
                    text = "My Self", fontWeight = FontWeight.SemiBold, color = Color.LightGray
                )
            }, leadingIcon = {
                RadioButton(selected = true, onClick = { /*TODO*/ })
            }, colors = OutlinedTextFieldDefaults.colors(
                unfocusedBorderColor = Color.LightGray,
                unfocusedContainerColor = MaterialTheme.colorScheme.background,
                disabledBorderColor = Color.LightGray
            ), enabled = false, modifier = Modifier.fillMaxWidth()
            )
            Spacer(modifier = Modifier.height(10.dp))
            CusTextField(viewModel.custId, "Customer ID", "Enter Customer Id")
            CusTextField(viewModel.firstName, "First Name", "Enter First Name")
            CusTextField(viewModel.lastName, "Last Name", "Enter Last Name")
            CusTextField(
                viewModel.dob,
                "DOB",
                "DD-MM-YYYY",
                trailingIcon = R.drawable.baseline_calendar_month_24,
                onClickTrailingIcon = {
                    datePickerDialogState.value = true
                },
                enabled = false
            )
            GenderSelectionField(label = "Gender", selectInt = viewModel.gender)
            CusTextField(
                viewModel.pincode,
                "Pin Code",
                "Enter PinCode",
                keyboardType = KeyboardType.Number,
                maxLength = 6
            )
            CusTextField(viewModel.address, "Address", "Enter Address")
            CusTextField(viewModel.district, "District", "Enter District")
            CusTextField(viewModel.state, "State", "Enter State")
            CusTextField(
                viewModel.emailId, "Email Id", "Enter Email Id", keyboardType = KeyboardType.Email
            )
            CusTextField(
                viewModel.phoneNo,
                "Phone No",
                "Enter Phone No.",
                keyboardType = KeyboardType.Phone,
                maxLength = 10
            )
            Spacer(modifier = Modifier.height(10.dp))
            WhatsappConfirmation(viewModel)
            Spacer(modifier = Modifier.height(16.dp))
            Row(Modifier.height(46.dp)) {
                Button(
                    onClick = {
                        navController.popBackStack()
                    }, modifier = Modifier.weight(1f), colors = ButtonDefaults.buttonColors(
                        containerColor = Color.LightGray
                    ), shape = RoundedCornerShape(8.dp)
                ) {
                    Text(
                        text = "Cancel",
                        Modifier
                            .fillMaxWidth()
                            .align(Alignment.CenterVertically),
                        textAlign = TextAlign.Center,
                        color = Color.Black
                    )
                }

                Spacer(modifier = Modifier.width(16.dp))
                Button(
                    onClick = {
                        if (checkDetails(viewModel)) {
                            viewModel.sendOTP(viewModel.emailId.value,
                                viewModel.phoneNo.value,
                                onSuccess = {
                                    showOtpDialog.value = true
                                },
                                onFailure = {
                                    val error = viewModel.failureMessage.value
                                    Toast.makeText(context, error, Toast.LENGTH_LONG).show()
                                })
                        } else {
                            Toast.makeText(
                                context,
                                "Please fill all the mandate fields.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }, modifier = Modifier.weight(1f), colors = ButtonDefaults.buttonColors(
                        containerColor = MaterialTheme.colorScheme.primary
                    ), shape = RoundedCornerShape(8.dp)
                ) {
                    Text(
                        text = "Proceed",
                        Modifier
                            .fillMaxWidth()
                            .align(Alignment.CenterVertically),
                        textAlign = TextAlign.Center,
                        color = Color.White
                    )
                }
            }

        }
    }

    // Decoupled snackbar host state from scaffold state for demo purposes.
    if (datePickerDialogState.value) DateRangePickerDialog(
        state = datePickerState, dialogState = datePickerDialogState, dateSelected = dateSelected
    ) {
        viewModel.dob.value = convertMillisToDate(
            datePickerState.selectedDateMillis!!, "yyyy-MM-dd"
        )
    }

    if (showOtpDialog.value) {
        OtpDialogScreen(navController = navController, viewModel = viewModel) {
            showOtpDialog.value = false
        }
    }
}


@Composable
fun CusTextField(
    value: MutableState<String>,
    label: String,
    placeHolder: String,
    keyboardType: KeyboardType = KeyboardType.Text,
    imeAction: ImeAction = ImeAction.Next,
    error: MutableState<Boolean> = mutableStateOf(false),
    errorText: MutableState<String> = mutableStateOf(""),
    trailingIcon: Int? = null,
    onClickTrailingIcon: () -> Unit = {},
    enabled: Boolean = true,
    maxLength: Int = 200
) {
    Column(Modifier.fillMaxWidth()) {
        Text(
            buildAnnotatedString {
                append(label)
                withStyle(style = SpanStyle(color = Color.Red)) {
                    append(" *")
                }
            }, fontWeight = FontWeight.SemiBold
        )
        Spacer(modifier = Modifier.height(10.dp))
        OutlinedTextField(
            enabled = enabled,
            value = value.value,
            onValueChange = {
                if (it.length <= maxLength) value.value = it
            },
            modifier = Modifier
                .fillMaxWidth()
                .clickable {
                    if (!enabled) {
                        onClickTrailingIcon()
                    }
                },
            colors = OutlinedTextFieldDefaults.colors(
                unfocusedBorderColor = Color.LightGray,
                unfocusedContainerColor = MaterialTheme.colorScheme.background,
                disabledTextColor = MaterialTheme.colorScheme.onBackground,
                disabledBorderColor = Color.LightGray
            ),
            placeholder = {
                Text(
                    text = placeHolder, color = Color.LightGray, fontWeight = FontWeight.SemiBold
                )
            },
            singleLine = true,
            trailingIcon = {
                if (trailingIcon != null) {
                    Icon(painter = painterResource(id = trailingIcon),
                        contentDescription = null,
                        tint = Color.LightGray,
                        modifier = Modifier.clickable {
                            onClickTrailingIcon()
                        })
                }
            },
            keyboardOptions = KeyboardOptions(
                keyboardType = keyboardType, imeAction = imeAction
            ),
        )

        if (error.value) {
            Text(
                text = errorText.value,
                color = MaterialTheme.colorScheme.error,
                fontSize = 14.sp,
                fontWeight = FontWeight.SemiBold,
                modifier = Modifier.padding(start = 8.dp)
            )
            Spacer(modifier = Modifier.height(4.dp))
        } else {
            Spacer(modifier = Modifier.height(16.dp))
        }
    }
}

@Composable
fun GenderSelectionField(label: String, selectInt: MutableState<Gender>) {
    Column(Modifier.fillMaxWidth()) {
        Text(
            buildAnnotatedString {
                append(label)
                withStyle(style = SpanStyle(color = MaterialTheme.colorScheme.error)) {
                    append(" *")
                }
            }, fontWeight = FontWeight.SemiBold
        )

        Spacer(modifier = Modifier.height(10.dp))
        Column(
            Modifier
                .fillMaxWidth()
                .border(1.dp, Color.LightGray, RoundedCornerShape(4.dp)),
            verticalArrangement = Arrangement.SpaceEvenly,
        ) {
            Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                RadioButton(selected = selectInt.value == Gender.FEMALE, onClick = {
                    selectInt.value = Gender.FEMALE
                })
                Text(text = "Female")
            }
            Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                RadioButton(selected = selectInt.value == Gender.MALE, onClick = {
                    selectInt.value = Gender.MALE
                })
                Text(text = "Male")
            }
            Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                RadioButton(selected = selectInt.value == Gender.OTHER, onClick = {
                    selectInt.value = Gender.OTHER
                })
                Text(text = "Other")
            }
        }
        Spacer(modifier = Modifier.height(10.dp))
    }
}

@Composable
fun WhatsappConfirmation(viewModel: HospicashViewModel) {
    Row(Modifier.fillMaxWidth()) {
        Checkbox(checked = viewModel.whatAppConsent.value, onCheckedChange = {
            viewModel.whatAppConsent.value = it
        }, modifier = Modifier.size(24.dp))
        Spacer(modifier = Modifier.width(6.dp))
        Image(
            painter = painterResource(id = R.drawable.whatsapp_icon),
            contentDescription = null,
            modifier = Modifier.size(24.dp)
        )
        Spacer(modifier = Modifier.width(6.dp))
        Text(
            text = "This number exist on whatsapp and customer agree to receive policy related communication over whatsapp.",
            Modifier.weight(1f),
            fontWeight = FontWeight.SemiBold,
            color = MaterialTheme.colorScheme.onBackground
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DateRangePickerDialog(
    state: DatePickerState,
    dialogState: MutableState<Boolean>,
    dateSelected: MutableState<Boolean>,
    onSave: () -> Unit
) {

    Box(
        Modifier
            .fillMaxSize()
            .background(TransparentBlack)
            .padding(30.dp),
        contentAlignment = Alignment.Center
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    color = MaterialTheme.colorScheme.background, RoundedCornerShape(10.dp)
                ), verticalArrangement = Arrangement.Top
        ) {
            // Add a row with "Save" and dismiss actions.
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 12.dp, end = 12.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                IconButton(onClick = {
                    dialogState.value = false
                }) {
                    Icon(Icons.Filled.Close, contentDescription = "Localized description")
                }
                TextButton(
                    onClick = {
                        onSave()
                        dialogState.value = false
                        dateSelected.value = true
                    }, enabled = state.selectedDateMillis != null
                ) {
                    Text(text = "Save")
                }
            }

            DatePicker(
                state = state, modifier = Modifier
                    .fillMaxWidth()
                    .padding(0.dp)
            )
        }
    }
}

@SuppressLint("SimpleDateFormat")
fun convertMillisToDate(millis: Long, pattern: String): String {
    val dateFormat = SimpleDateFormat(pattern)
    val date = Date(millis)
    return dateFormat.format(date)
}


fun checkDetails(viewModel: HospicashViewModel): Boolean {
    if (viewModel.custId.value.isBlank()) return false
    if (viewModel.custId.value.isBlank()) return false
    if (viewModel.firstName.value.isBlank()) return false
    if (viewModel.lastName.value.isBlank()) return false
    if (viewModel.dob.value.isBlank()) return false
    if (viewModel.pincode.value.isBlank()) return false
    if (viewModel.address.value.isBlank()) return false
    if (viewModel.district.value.isBlank()) return false
    if (viewModel.state.value.isBlank()) return false
    if (viewModel.emailId.value.isBlank()) return false
    if (viewModel.phoneNo.value.isBlank()) return false
    if (viewModel.phoneNo.value.length != 10) return false
    return true
}

enum class Gender(name: String) {
    FEMALE("Female"), MALE("Male"), OTHER("Other")
}