package com.isu.hospicashinapp

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.lifecycleScope
import androidx.navigation.compose.rememberNavController
import com.isu.hospicashinapp.presenter.FailureDialog
import com.isu.hospicashinapp.presenter.HospicashNavigation
import com.isu.hospicashinapp.ui.theme.HospicashTheme
import com.prepaid_service_app.components.CustomAlertDialog
import com.velox.lazeir.utils.internetConnectivityListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay

@AndroidEntryPoint
class HospicashLaunchActivity : ComponentActivity() {
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            HospicashTheme {
                val isInternetConnected = remember{
                    mutableStateOf(false)
                }
                Surface(
                    modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background
                ) {

//                    Toast.makeText(this, "${HospicashRequiredData.access_token}", Toast.LENGTH_LONG).show()

                    internetConnectivityListener(
                        lifecycleScope = lifecycleScope,
                        applicationContext = this@HospicashLaunchActivity,
                        onAvailable = {
                                      isInternetConnected.value  = true
                        },
                        onLosing = {
                                   isInternetConnected.value = false
                        },
                        onLost = {
                                 isInternetConnected.value =  false
                        },
                        onUnAvailable = {
                                        isInternetConnected.value = false
                        },
                    )


                    val navController = rememberNavController()
                    val viewModel = hiltViewModel<HospicashViewModel>()

//                    LaunchedEffect(key1 = viewModel.failureMessage.value) {
//                        viewModel.failureStatus.value = viewModel.failureMessage.value.isNotBlank()
//                        delay(5000)
//                        viewModel.failureMessage.value=""
//                    }

                    HospicashNavigation(navController, viewModel)

                    if (!isInternetConnected.value) {
                        Dialog(
                            onDismissRequest = { }, properties = DialogProperties(
                                dismissOnBackPress = false,
                                dismissOnClickOutside = false
                            )
                        ) {
                            Card(
                                modifier = Modifier
                                    .width(200.dp)
                                    .height(150.dp)
                            ) {
                                Column(
                                    modifier = Modifier.fillMaxSize(),
                                    verticalArrangement = Arrangement.Center,
                                    horizontalAlignment = Alignment.CenterHorizontally
                                ) {
                                    Icon(
                                        painter = painterResource(id = com.google.android.material.R.drawable.mtrl_ic_error),
                                        contentDescription = null
                                    )
                                    Text("No Connection")

                                }
                            }
                        }
                    }

                    if (viewModel.failureStatus.value) {
                        FailureDialog(viewModel = viewModel)
                    }

                    if (viewModel.loading.value){
                        //todo set loading dialog
                        AlertDialog(onDismissRequest = {  },modifier = Modifier.size(40.dp)) {
                            CircularProgressIndicator(modifier = Modifier.size(40.dp))

                        }
                    }
                }
            }
        }
    }
}

