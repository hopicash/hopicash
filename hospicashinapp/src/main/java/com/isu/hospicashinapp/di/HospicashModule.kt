package com.isu.hospicashinapp.di

import com.isu.hospicashinapp.data.network.ApiService
import com.isu.hospicashinapp.data.repo.RepositoryImpl
import com.isu.hospicashinapp.domain.Repository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object HospicashModule {
    @Provides
    @Singleton
    fun provideRetrofitBuilder(): ApiService {
        val loggingInterceptor =
            HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        val client: OkHttpClient = OkHttpClient.Builder().connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .addInterceptor(loggingInterceptor)
            .retryOnConnectionFailure(true)
            .build()
        return Retrofit.Builder().baseUrl("https://apidev.iserveu.online/stagingauth/")
            .addConverterFactory(GsonConverterFactory.create())
            .addConverterFactory(ScalarsConverterFactory.create()).client(client).build()
            .create(ApiService::class.java)
    }


    @Provides
    @Singleton
    fun provideRepo(apiService: ApiService):Repository{
        return RepositoryImpl(apiService)
    }

}