package com.isu.hospicashinapp.domain

import com.isu.hospicashinapp.data.model.req.CreateOrderRequest
import com.isu.hospicashinapp.data.model.req.OrderStatusRequest
import com.isu.hospicashinapp.data.model.req.PincodeFetchRequest
import com.isu.hospicashinapp.data.model.req.ProductDetailsRequest
import com.isu.hospicashinapp.data.model.req.SendOtpRequestBody
import com.isu.hospicashinapp.data.model.req.ValidateOtpRequestBody
import com.isu.hospicashinapp.data.model.res.CreateOrderResponse
import com.isu.hospicashinapp.data.model.res.OrderPdfResponse
import com.isu.hospicashinapp.data.model.res.OrderStatusResponse
import com.isu.hospicashinapp.data.model.res.OtpResponseBody
import com.isu.hospicashinapp.data.model.res.PincodeFetchResponse
import com.isu.hospicashinapp.data.model.res.ProductCoverAmountResponse
import com.isu.hospicashinapp.data.model.res.ProductDetailsResponse
import com.velox.lazeir.utils.handler.NetworkResource
import kotlinx.coroutines.flow.Flow

interface Repository {

    suspend fun productCoverAmount(
        token: String,
    ): Flow<NetworkResource<ProductCoverAmountResponse>>

    suspend fun productDetails(
        token: String,
        body: ProductDetailsRequest
    ): Flow<NetworkResource<ProductDetailsResponse>>

    suspend fun createOrder(
        token: String,
        body: CreateOrderRequest
    ): Flow<NetworkResource<CreateOrderResponse>>

    suspend fun orderStatus(
        token: String,
        body: OrderStatusRequest
    ): Flow<NetworkResource<OrderStatusResponse>>

    suspend fun sendOtp(
        token: String,
        body: SendOtpRequestBody
    ): Flow<NetworkResource<OtpResponseBody>>

    suspend fun reSendOtp(
        token: String,
        body: SendOtpRequestBody
    ): Flow<NetworkResource<OtpResponseBody>>

    suspend fun ValidateOtp(
        token: String,
        body: ValidateOtpRequestBody
    ): Flow<NetworkResource<OtpResponseBody>>

    suspend fun pincodeFetch(
        token: String,
        body: PincodeFetchRequest
    ): Flow<NetworkResource<PincodeFetchResponse>>

    suspend fun orderPDF(
        token: String,
        body: OrderStatusRequest
    ): Flow<NetworkResource<OrderPdfResponse>>
}