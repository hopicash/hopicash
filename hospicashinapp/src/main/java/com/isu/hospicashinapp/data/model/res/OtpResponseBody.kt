package com.isu.hospicashinapp.data.model.res

data class OtpResponseBody(
    val message: String,
    val status: Int
)