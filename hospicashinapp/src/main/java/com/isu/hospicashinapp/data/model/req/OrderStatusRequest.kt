package com.isu.hospicashinapp.data.model.req

import com.google.gson.annotations.SerializedName

data class OrderStatusRequest(

	@field:SerializedName("orderRefNo")
	val orderRefNo: String? = null
)
