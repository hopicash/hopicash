package com.isu.hospicashinapp.data.model.req

data class SendOtpRequestBody(
    val email: String,
    val mobileNo: String
)