package com.isu.hospicashinapp.data.model.req

import com.google.gson.annotations.SerializedName

data class CreateOrderRequest(

	@field:SerializedName("watsAppConsent")
	val watsAppConsent: Boolean? = null,

	@field:SerializedName("memberList")
	val memberList: List<MemberListItem?>? = null,

	@field:SerializedName("customer")
	val customer: Customer? = null
){
	data class MemberListItem(

		@field:SerializedName("insuredMobile")
		val insuredMobile: String? = null,

		@field:SerializedName("gender")
		val gender: String? = null,

		@field:SerializedName("insuredEmail")
		val insuredEmail: String? = null,

		@field:SerializedName("memberName")
		val memberName: String? = null,

		@field:SerializedName("dateOfBirth")
		val dateOfBirth: String? = null,

		@field:SerializedName("relation")
		val relation: String? = null
	)
	data class Customer(

		@field:SerializedName("firstName")
		val firstName: String? = null,

		@field:SerializedName("lastName")
		val lastName: String? = null,

		@field:SerializedName("pincode")
		val pincode: String? = null,

		@field:SerializedName("address")
		val address: String? = null,

		@field:SerializedName("custdateOfBirth")
		val custdateOfBirth: String? = null,

		@field:SerializedName("mobile")
		val mobile: String? = null,

		@field:SerializedName("gender")
		val gender: String? = null,

		@field:SerializedName("emailId")
		val emailId: String? = null
	)
}
