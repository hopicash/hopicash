package com.isu.hospicashinapp.data.model.req

import com.google.gson.annotations.SerializedName

data class ProductDetailsRequest(

	@field:SerializedName("sumInsured")
	val sumInsured: List<Int?>? = null
)
