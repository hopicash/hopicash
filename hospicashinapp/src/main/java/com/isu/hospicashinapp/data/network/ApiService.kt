package com.isu.hospicashinapp.data.network

import com.isu.hospicashinapp.data.model.req.CreateOrderRequest
import com.isu.hospicashinapp.data.model.req.OrderStatusRequest
import com.isu.hospicashinapp.data.model.req.PincodeFetchRequest
import com.isu.hospicashinapp.data.model.req.ProductDetailsRequest
import com.isu.hospicashinapp.data.model.req.SendOtpRequestBody
import com.isu.hospicashinapp.data.model.req.ValidateOtpRequestBody
import com.isu.hospicashinapp.data.model.res.CreateOrderResponse
import com.isu.hospicashinapp.data.model.res.OrderPdfResponse
import com.isu.hospicashinapp.data.model.res.OrderStatusResponse
import com.isu.hospicashinapp.data.model.res.OtpResponseBody
import com.isu.hospicashinapp.data.model.res.PincodeFetchResponse
import com.isu.hospicashinapp.data.model.res.ProductCoverAmountResponse
import com.isu.hospicashinapp.data.model.res.ProductDetailsResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface ApiService {

    //    @Headers("Content-type: application/json")
    @POST("/stagingauth/hospicash/productCoverAmount")
    suspend fun productCoverAmount(
//        @Url url: String,
        @Header("Authorization") token: String,
//        @Header("token_properties") tokenProperties:String = """{"userName":"itpl","bankCode":"common","adminName":"technewadmin","role":["ROLE_ADMIN"],"privileges":["KYC_ONBOARD","RETAILER_BLOCK_UNBLOCK","RETAILER_CRUD","MASTER_DISTRIBUTOR_BLOCK_UNBLOCK","DISTRIBUTOR_BLOCK_UNBLOCK","DISTRIBUTOR_CRUD","ROLE_MANAGEMET","MASTER_DISTRIBUTOR_CRUD","PLAN_MANAGEMENT_CRUD","PLAN_MANAGEMENT_VIEW","KYC_UPDATE","DEVICE_MANAGEMENT"]}""".trimIndent()
    ): Response<ProductCoverAmountResponse>

    @POST("/stagingauth/hospicash/productDetails")
    suspend fun productDetails(
        @Header("Authorization") token: String,
        @Body body: ProductDetailsRequest
    ): Response<ProductDetailsResponse>


    @POST("/stagingauth/hospicash/createOrder")
    suspend fun createOrder(
        @Header("Authorization") token: String,
        @Body body: CreateOrderRequest
    ): Response<CreateOrderResponse>

    @POST("/stagingauth/hospicash/orderStatus")
    suspend fun orderStatus(
        @Header("Authorization") token: String,
        @Body body: OrderStatusRequest
    ): Response<OrderStatusResponse>

    @POST("/stagingauth/hospicash/orderPDF")
    suspend fun orderPDF(
        @Header("Authorization") token: String,
        @Body body: OrderStatusRequest
    ): Response<OrderPdfResponse>

    @POST("/stagingauth/hospicash/sendOtp")
    suspend fun sendOtp(
        @Header("Authorization") token: String,
        @Body body: SendOtpRequestBody
    ) : Response<OtpResponseBody>

    @POST("/stagingauth/hospicash/resendOtp")
    suspend fun reSendOtp(
        @Header("Authorization") token: String,
        @Body body: SendOtpRequestBody
    ) : Response<OtpResponseBody>

    @POST("/stagingauth/hospicash/validateOtp")
    suspend fun validateOtp(
        @Header("Authorization") token: String,
        @Body body: ValidateOtpRequestBody
    ) : Response<OtpResponseBody>

    @POST("/stagingauth/hospicash/pincodeFetch")
    suspend fun pincodeFetch(
        @Header("Authorization") token: String,
        @Body body: PincodeFetchRequest
    ) : Response<PincodeFetchResponse>

}