package com.isu.hospicashinapp.data.model.res

import com.google.gson.annotations.SerializedName

data class ProductDetailsResponse(

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
){
	data class DataItem(

		@field:SerializedName("longDescription")
		val longDescription: String? = null,

		@field:SerializedName("serviceProviderId")
		val serviceProviderId: Int? = null,

		@field:SerializedName("policyUIN")
		val policyUIN: String? = null,

		@field:SerializedName("shortDescriptionArray")
		val shortDescriptionArray: List<ShortDescriptionArrayItem?>? = null,

		@field:SerializedName("childMaxAge")
		val childMaxAge: Int? = null,

		@field:SerializedName("productName")
		val productName: String? = null,

		@field:SerializedName("extrainfo")
		val extrainfo: String? = null,

		@field:SerializedName("finalPremium")
		val finalPremium: Int? = null,

		@field:SerializedName("numberOfHospitals")
		val numberOfHospitals: Int? = null,

		@field:SerializedName("policyTenure")
		val policyTenure: String? = null,

		@field:SerializedName("productImage")
		val productImage: String? = null,

		@field:SerializedName("sumInsured")
		val sumInsured: Int? = null,

		@field:SerializedName("productSKUId")
		val productSKUId: Int? = null,

		@field:SerializedName("imageBaseUrl")
		val imageBaseUrl: String? = null,

		@field:SerializedName("productPricingId")
		val productPricingId: Int? = null,

		@field:SerializedName("partnerProductName")
		val partnerProductName: Any? = null,

		@field:SerializedName("productType")
		val productType: String? = null,

		@field:SerializedName("otpTemplateName")
		val otpTemplateName: String? = null,

		@field:SerializedName("productId")
		val productId: Int? = null,

		@field:SerializedName("subCategoryId")
		val subCategoryId: Int? = null,

		@field:SerializedName("logoURL")
		val logoURL: String? = null,

		@field:SerializedName("childMinAge")
		val childMinAge: Any? = null,

		@field:SerializedName("adultMaxAge")
		val adultMaxAge: Int? = null,

		@field:SerializedName("productCode")
		val productCode: String? = null,

		@field:SerializedName("adultMinAge")
		val adultMinAge: Int? = null,

		@field:SerializedName("masterPolicyNumber")
		val masterPolicyNumber: String? = null,

		@field:SerializedName("categoryId")
		val categoryId: Int? = null
	){
		data class ShortDescriptionArrayItem(

			@field:SerializedName("value")
			val value: String? = null
		)

	}
}
