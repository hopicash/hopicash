package com.isu.hospicashinapp.data.repo

import com.isu.hospicashinapp.data.model.req.CreateOrderRequest
import com.isu.hospicashinapp.data.model.req.OrderStatusRequest
import com.isu.hospicashinapp.data.model.req.PincodeFetchRequest
import com.isu.hospicashinapp.data.model.req.ProductDetailsRequest
import com.isu.hospicashinapp.data.model.req.SendOtpRequestBody
import com.isu.hospicashinapp.data.model.req.ValidateOtpRequestBody
import com.isu.hospicashinapp.data.model.res.CreateOrderResponse
import com.isu.hospicashinapp.data.model.res.OrderPdfResponse
import com.isu.hospicashinapp.data.model.res.OrderStatusResponse
import com.isu.hospicashinapp.data.model.res.OtpResponseBody
import com.isu.hospicashinapp.data.model.res.PincodeFetchResponse
import com.isu.hospicashinapp.data.model.res.ProductCoverAmountResponse
import com.isu.hospicashinapp.data.model.res.ProductDetailsResponse
import com.isu.hospicashinapp.data.network.ApiService
import com.isu.hospicashinapp.domain.Repository
import com.velox.lazeir.utils.handler.NetworkResource
import com.velox.lazeir.utils.outlet.handleNetworkResponse
import kotlinx.coroutines.flow.Flow

class RepositoryImpl(private val apiService: ApiService) : Repository {
    override suspend fun productCoverAmount(token: String): Flow<NetworkResource<ProductCoverAmountResponse>> {
        return apiService.productCoverAmount(token = token).handleNetworkResponse()
    }

    override suspend fun productDetails(
        token: String, body: ProductDetailsRequest
    ): Flow<NetworkResource<ProductDetailsResponse>> {
        return apiService.productDetails(token, body).handleNetworkResponse()
    }

    override suspend fun sendOtp(
        token: String,
        body: SendOtpRequestBody
    ): Flow<NetworkResource<OtpResponseBody>> {
        return apiService.sendOtp(token, body).handleNetworkResponse()
    }

    override suspend fun reSendOtp(
        token: String,
        body: SendOtpRequestBody
    ): Flow<NetworkResource<OtpResponseBody>> {
        return apiService.reSendOtp(token, body).handleNetworkResponse()
    }

    override suspend fun ValidateOtp(
        token: String,
        body: ValidateOtpRequestBody
    ): Flow<NetworkResource<OtpResponseBody>> {
       return apiService.validateOtp(token, body).handleNetworkResponse()
    }

    override suspend fun createOrder(
        token: String,
        body: CreateOrderRequest
    ): Flow<NetworkResource<CreateOrderResponse>> {
        return apiService.createOrder(token, body).handleNetworkResponse()
    }

    override suspend fun orderStatus(
        token: String,
        body: OrderStatusRequest
    ): Flow<NetworkResource<OrderStatusResponse>> {
        return apiService.orderStatus(token, body).handleNetworkResponse()
    }

    override suspend fun pincodeFetch(
        token: String,
        body: PincodeFetchRequest
    ): Flow<NetworkResource<PincodeFetchResponse>> {
        return apiService.pincodeFetch(token, body).handleNetworkResponse()
    }

    override suspend fun orderPDF(
        token: String,
        body: OrderStatusRequest
    ): Flow<NetworkResource<OrderPdfResponse>> {
        return apiService.orderPDF(token, body).handleNetworkResponse()
    }
}