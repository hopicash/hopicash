package com.isu.hospicashinapp.data.model.req

import com.google.gson.annotations.SerializedName

data class PincodeFetchRequest(

	@field:SerializedName("pincode")
	val pincode: String? = null
)
