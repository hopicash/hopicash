package com.isu.hospicashinapp.data.model.req

data class ValidateOtpRequestBody(
    val mobileNo: String?,
    val otp: String?
)