package com.isu.hospicashinapp.data.model.res

import com.google.gson.annotations.SerializedName

data class ProductCoverAmountResponse(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
){
	data class Data(

		@field:SerializedName("maxChildAllowed")
		val maxChildAllowed: Int? = null,

		@field:SerializedName("maxAdultAllowed")
		val maxAdultAllowed: Int? = null,

		@field:SerializedName("productCoverAmounts")
		val productCoverAmounts: List<ProductCoverAmountsItem?>? = null
	){
		data class ProductCoverAmountsItem(

			@field:SerializedName("coverAmtExt")
			val coverAmtExt: Any? = null,

			@field:SerializedName("sImaxChildAllowed")
			val sImaxChildAllowed: Int? = null,

			@field:SerializedName("sImaxAdultAllowed")
			val sImaxAdultAllowed: Int? = null,

			@field:SerializedName("id")
			val id: Int? = null,

			@field:SerializedName("coverAmount")
			val coverAmount: Int? = null
		)
	}
}
