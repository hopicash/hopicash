package com.isu.hospicashinapp.data.model.res

import com.google.gson.annotations.SerializedName

data class CreateOrderResponse(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
){
	data class Data(

		@field:SerializedName("ProductInfo")
		val productInfo: String? = null,

		@field:SerializedName("PaymentRefNumber")
		val paymentRefNumber: String? = null,

		@field:SerializedName("Email")
		val email: String? = null,

		@field:SerializedName("FirstName")
		val firstName: String? = null,

		@field:SerializedName("Amount")
		val amount: Int? = null,

		@field:SerializedName("PolicyRefNumber")
		val policyRefNumber: String? = null,

		@field:SerializedName("OrderNo")
		val orderNo: String? = null,

		@field:SerializedName("ApplicationCode")
		val applicationCode: String? = null,

		@field:SerializedName("LoginUserId")
		val loginUserId: String? = null,

		@field:SerializedName("OrderStatus")
		val orderStatus: Any? = null,

		@field:SerializedName("Phone")
		val phone: String? = null,

		@field:SerializedName("OrderRefNumber")
		val orderRefNumber: String? = null,

		@field:SerializedName("ProposalNumber")
		val proposalNumber: String? = null,

		@field:SerializedName("LastName")
		val lastName: String? = null,

		@field:SerializedName("BaseUrl")
		val baseUrl: String? = null
	)
}
