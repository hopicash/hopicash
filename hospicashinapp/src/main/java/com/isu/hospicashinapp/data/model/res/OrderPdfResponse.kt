package com.isu.hospicashinapp.data.model.res

import com.google.gson.annotations.SerializedName

data class OrderPdfResponse(

	@field:SerializedName("data")
	val data: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
