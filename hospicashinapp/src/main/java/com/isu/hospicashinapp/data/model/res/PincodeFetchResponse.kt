package com.isu.hospicashinapp.data.model.res

import com.google.gson.annotations.SerializedName

data class PincodeFetchResponse(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
){
	data class Data(

		@field:SerializedName("stateName")
		val stateName: String? = null,

		@field:SerializedName("city")
		val city: String? = null,

		@field:SerializedName("pinCode")
		val pinCode: String? = null
	)
}
