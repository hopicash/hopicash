package com.isu.hospicashinapp

class HospicashRequiredData {
    companion object{
        var bankCode:String? = ""
        var adminName:String? = ""
        var access_token : String? = ""
        var mobileNumber:String? = ""
    }
}