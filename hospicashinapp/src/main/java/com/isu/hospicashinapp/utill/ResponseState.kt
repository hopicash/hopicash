package com.isu.hospicashinapp.utill

data class ResponseState(
    val isLoading:Boolean = false,
    val isError:Boolean = false,
    val isSuccess:Boolean = false,
)
